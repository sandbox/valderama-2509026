<?php
/**
 * @file
 * Contains \Drupal\event\EventInterface.
 */

namespace Drupal\event;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Contact entity.
 */
interface EventInterface extends ContentEntityInterface/*, EntityOwnerInterface*/ {

}
