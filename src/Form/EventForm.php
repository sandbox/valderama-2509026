<?php

namespace Drupal\event\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

class EventForm extends ContentEntityForm {


  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\event\Entity\Event */

    $form = [];
    $form = parent::buildForm($form, $form_state);

    return $form;
  }


  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->data->value = ['This is some data'];
    $entity->save();
    //parent::save($form, $form_state);

    $form_state->setRedirect('entity.event.collection');
  }
}
